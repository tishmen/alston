'''utils.py'''

class SQL(object):

    '''PostgreSQL upsert transaction statement.'''

    def __init__(self, data):
        '''Set item dict and upsert SQL.'''
        self.data = data
        self.sql = 'INSERT INTO {} ({}) VALUES ({}) ON CONFLICT (uid) DO UPDAT'\
            'E SET ({}) = ({}); '

    def insert(self, data):
        '''Return insert part of SQL upsert statement.'''
        values = ["'{}'".format(v.replace("'", "''")) for v in data.values()]
        return ', '.join(data.keys()), ', '.join(values)

    def update(self, data):
        '''Return update part of SQL upsert statement.'''
        data.pop('uid')
        data['timestamp'] = 'now()'
        return (
            ', '.join(data.keys()),
            ', '.join(['EXCLUDED.{}'.format(k) for k in data.keys()])
        )

    def upsert(self, table, data):
        '''Return combined upsert SQL statement'''
        return self.sql.format(table, *self.insert(data), *self.update(data))

    def generate(self):
        '''Generate SQL upsert transaction statement.'''
        offices = self.data.pop('offices', [])
        profile = self.data
        profile_id = profile['uid']
        sql = 'BEGIN; '
        sql += self.upsert('profiles', profile)
        for office in offices:
            office['profile_uid'] = profile_id
            sql += self.upsert('offices', office)
        return sql + 'COMMIT;'
