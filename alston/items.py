'''items.py'''

from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Identity


class ProfileItem(Item):

    '''Profile item.'''

    uid = Field()
    url = Field()
    name = Field()
    level = Field()
    photo = Field()
    phone1 = Field()
    phone2 = Field()
    offices = Field()


class OfficeItem(Item):

    '''Office item.'''

    uid = Field()
    name = Field()
    url = Field()
    photo = Field()
    title = Field()
    level = Field()



class ProfileItemLoader(ItemLoader):

    '''Quote item loader.'''

    default_item_class = ProfileItem
    default_output_processor = TakeFirst()
    offices_out = Identity()


class OfficeItemLoader(ItemLoader):

    '''Office item loader.'''

    default_item_class = OfficeItem
    default_output_processor = TakeFirst()
