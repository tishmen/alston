'''profiles.py'''

import json

from fake_useragent import UserAgent
from scrapy import Spider, Request

from alston.items import ProfileItemLoader, OfficeItemLoader


class ProfilesSpider(Spider):

    '''Profiles spider.'''

    name = 'profiles'

    def __init__(self, *args, **kwargs):
        '''Set random user agent.'''
        super(ProfilesSpider, self).__init__(*args, **kwargs)
        self.useragent = UserAgent().random

    def start_requests(self):
        '''Initial request.'''
        url = 'https://www.alston.com/en/professionals/'
        headers = {'User-Agent': self.useragent}
        yield Request(
            url, headers=headers, callback=self.parse, errback=self.errback
        )

    def errback(self, failure):
        '''Log error.'''
        self.logger.error(str(failure))

    def get_cookies(self, response):
        '''Return LB_LastVisitedCookie cookie.'''
        cookie = response.headers[b'Set-Cookie'].decode('utf-8')
        return {'LB_LastVisitedCookie': cookie.split('=')[1].split(';')[0]}

    def get_body(self, response):
        '''Return search post data.'''
        selector = '//script[contains(text(), "var initialJsonData = ")]/text()'
        text = response.xpath(selector).extract_first()
        text = text.split('var initialJsonData = "')[1][:-3]
        text = text.encode('utf-8').decode('unicode_escape')
        return json.dumps({'Take': json.loads(text)['ResultCount']})

    def parse(self, response):
        '''Parse profiles page.'''
        url = 'https://www.alston.com/api/sitecore/professionals/search'
        headers = {
            'User-Agent': self.useragent, 'Content-Type': 'application/json'
        }
        yield Request(
            url,
            method='POST',
            headers=headers,
            cookies=self.get_cookies(response),
            body=self.get_body(response),
            callback=self.parse_search,
            errback=self.errback
        )

    def load_profile(self, node):
        '''Load profile item.'''
        loader = ProfileItemLoader()
        loader.add_value('uid', node['ID'])
        loader.add_value('url', node['Url'])
        loader.add_value('name', node['Name'])
        loader.add_value('level', node['AttorneyLevelTitle'])
        loader.add_value('photo', node['PhotoUrl'])
        loader.add_value('phone1', node['BusinessPhone'])
        loader.add_value('phone2', node['OtherPhone'])
        return loader

    def load_office(self, node):
        '''Load office item.'''
        loader = OfficeItemLoader()
        loader.add_value('uid', node['ID'])
        loader.add_value('name', node['Name'])
        loader.add_value('url', node['Url'])
        loader.add_value('photo', node['PhotoUrl'])
        loader.add_value('title', node['AttorneyTitle'])
        loader.add_value('level', node['AttorneyLevel'])
        return loader

    def parse_search(self, response):
        '''Parse search endpoint.'''
        for profile in json.loads(response.body_as_unicode())['GridData']:
            profile_loader = self.load_profile(profile)
            offices = []
            for office in profile['Offices']:
                office_loader = self.load_office(office)
                offices.append(dict(office_loader.load_item()))
            profile_loader.add_value('offices', offices)
            yield profile_loader.load_item()
