'''settings.py'''

import os


BOT_NAME = 'alston'
SPIDER_MODULES = ['alston.spiders']
ITEM_PIPELINES = {'alston.pipelines.PostgreSQLPipeline': 100}

POSTGRESQL_HOST = os.getenv('POSTGRESQL_HOST')
POSTGRESQL_PORT = int(os.getenv('POSTGRESQL_PORT'))
POSTGRESQL_USER = os.getenv('POSTGRESQL_USER')
POSTGRESQL_PASS = os.getenv('POSTGRESQL_PASS')
POSTGRESQL_NAME = os.getenv('POSTGRESQL_NAME')
