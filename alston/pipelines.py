'''pipelines.py'''

from copy import deepcopy

from twisted.enterprise import adbapi
from twisted.internet.defer import inlineCallbacks, returnValue

from .utils import SQL


class PostgreSQLPipeline(object):

    '''PostgreSQL database pipeline.'''

    def __init__(self, host, port, user, password, database):
        '''Initialize database connection pool.'''
        self.dbpool = adbapi.ConnectionPool(
            'psycopg2',
            host=host,
            port=port,
            user=user,
            password=password,
            database=database
        )

    @classmethod
    def from_crawler(cls, crawler):
        '''Pass database settings to constructor.'''
        return cls(
            crawler.settings.get('POSTGRESQL_HOST'),
            crawler.settings.get('POSTGRESQL_PORT'),
            crawler.settings.get('POSTGRESQL_USER'),
            crawler.settings.get('POSTGRESQL_PASS'),
            crawler.settings.get('POSTGRESQL_NAME')
        )

    @staticmethod
    def execute_sql(cursor, item):
        '''Execute SQL statement.'''
        cursor.execute(SQL(item).generate())

    @inlineCallbacks
    def process_item(self, item, *args, **kwargs):
        '''Run database interaction and return item for further processing.'''
        yield self.dbpool.runInteraction(self.execute_sql, deepcopy(dict(item)))
        returnValue(item)

    def close_spider(self, *args, **kwargs):
        '''Destroy database connection pool.'''
        self.dbpool.close()
