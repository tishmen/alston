# Alston demo project

A sample project for extracting profile info from https://www.alston.com using Scrapy, Scrapyd and plain HTTP requests.

Collected profile data contains the following fields: id, url, name, level, photo, business phone and other phone. Also data about offices in which a professional is registered is collected. Office data contains the following fields: id, name, url, photo, title and level.

# Running locally

To run the project locally using Scrapy issue:

```sh
scrapy crawl profiles
```

To run the project using Scrapyd locally, first deploy the project using:

```sh
scrapyd deploy
```

Than send request to Scrapyd API running on localhost with command:

```sh
curl http://localhost:6800/schedule.json -d project=alston -d spider=profiles
```

You can inspect the status, items and logs at Scrapyd web interface at http://localhost:6800/jobs.

# Running on AWS

To deploy the project on AWS EC2 instance, start an EC2 instance with ports 22 and 6800 open. Ssh to the container and pull this repository.

```sh
ssh -i scrapyd.pem ubuntu@<ec2-ip>
git clone https://tishmen@bitbucket.org/tishmen/alston.git
```

Change directory to alston/deployment and issue:

```sh
cd alston/deployment
sudo ./provision.sh
```

After that Scrapyd web interface and API should be live and available at:

```sh
http://<ec2-ip>:6800
```

# TODO

* Write profiles spider tests using scrapy contracts.
* Use boto3 to create and provision EC2 instance.
