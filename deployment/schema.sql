CREATE TABLE profiles (
  uid varchar(36) PRIMARY KEY,
  url text NOT NULL,
  photo text,
  name varchar(100) NOT NULL,
  level varchar(250) NOT NULL,
  phone1 varchar(100) NOT NULL,
  phone2 varchar(100),
  timestamp timestamp DEFAULT current_timestamp
);
CREATE TABLE offices (
  profile_uid varchar(36) REFERENCES profiles(uid) NOT NULL,
  uid varchar(36) PRIMARY KEY,
  name varchar(100) NOT NULL,
  url text NOT NULL,
  photo text NOT NULL,
  title varchar(250),
  level varchar(250),
  timestamp timestamp DEFAULT current_timestamp
);
