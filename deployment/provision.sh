#!/bin/bash


# Better print function

function _print() {
  echo
  echo "#######################################################################"
  echo "# $1"
  echo "#######################################################################"
  echo
}


# Check user permissions

function check_root() {
  if [[ $EUID > 0 ]]; then
    _print "Please run script as root/sudo user."
    exit 1
  fi
  _print "Running script as root/sudo user."
}

check_root


# Generate random 32 character password

function _generate_password() {
  < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c32 ; echo
}


# Install system dependencies

function configure_system() {
  PACKAGES=(
    postgresql
    postgresql-contrib
    supervisor
    python3
    python3-dev
    python3-pip
    libxml2-dev
    libxslt1-dev
    zlib1g-dev
    libffi-dev
    libssl-dev
  )
  apt-get update && apt-get install -y ${PACKAGES[@]}
  _print "Installed system dependencies."
}


# Setup database with tables and user with password and privileges

DATABASE="alston"
USER=$DATABASE
PASSWORD="$(_generate_password)"

function configure_database() {
  CREATE_DATABASE="CREATE DATABASE $DATABASE;"
  CREATE_USER="CREATE USER $USER WITH ENCRYPTED PASSWORD '$PASSWORD';"
  GRANT_ALL="GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO $USER;"
  SCHEMA=$(<schema.sql)
  sudo -u postgres psql -c "$CREATE_DATABASE"
  _print "Created database."
  sudo -u postgres psql -c "$CREATE_USER"
  _print "Created database user."
  sudo -u postgres psql -d $DATABASE -c "$SCHEMA"
  _print "Created database tables."
  sudo -u postgres psql -d $DATABASE -c "$GRANT_TABLES"
  _print "Created database privileges."
}


# Install python dependencies

function configure_python() {
  pip3 install -r requirements.txt
  _print "Installed python dependencies."
}


# Start supervisord with environment variables and deploy project

function configure_scrapyd() {
  cp scrapyd.conf /etc/supervisor/conf.d/scrapyd.conf
  echo "environment=
  POSTGRESQL_HOST=127.0.0.1,
  POSTGRESQL_PORT=5432,
  POSTGRESQL_NAME=$DATABASE,
  POSTGRESQL_USER=$USER,
  POSTGRESQL_PASS=$PASSWORD" >> /etc/supervisor/conf.d/scrapyd.conf
  supervisorctl reread && supervisorctl update
  _print "Scrapyd running."
  scrapyd-deploy
  _print "Scrapy project deployed."
}


# Run functions from script

function main() {
  configure_system
  configure_database
  configure_environment
  configure_python
  configure_scrapyd
  _print "Script finished."
}

main
